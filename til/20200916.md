# NITI Aayog

- involvement of State Govt
- bottom-up (?) policy making
- "15-year road map", "7-year vision, strategy, and action plan"
- council comprises of
  - All CMs (including Delhi and Puducherry)
  - Lieutenant Governor of Andaman and Nicobar
  - Vice chairmen
  - temp members selected from top universities and research institutes
- Former Finance Minister Arun Jaitley made the following observation on the necessity of creating NITI Aayog,

    "The 65-year-old Planning Commission had become a redundant organisation. It was relevant in a command economy structure, but not any longer. India is a diversified country and its states are in various phases of economic development along with their own strengths and weaknesses. In this context, a ‘one size fits all’ approach to economic planning is obsolete. It cannot make India competitive in today's global economy."[3] It is a reformation scheme of the day-to-day lifestyles of the people of India.



# Planning Commission

- top-down (?) approach
