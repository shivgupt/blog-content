# About Me 😇

My name is Shiv. I'm lazy and that's why I like making computers do the work.

I respect Intellectual Property and patents, but I ❤️ Open Source. That was the main reason I quit my corporate American job, back in 2018, and have only worked on developing Open Source Software since then.

Lately I have been involved in the blockchain space - mainly Ethereum, building DeFi (Decentralized Finance) tools.

This blog is still a work in progress and will be the place for me to consolidate my research notes, rants, learnings and media.

I'm a little bit of a control freak, especially when it comes to my data and this is my attempt to share my thoughts with family, friends and rest of the world via a medium owned by myself and not censored by random tech administrator on some social media platform.