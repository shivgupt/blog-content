One of the reservation I have with Sharia law is about its position on property ownership wrt to women. 

Q 4:34
```
Men are in charge of women by [right of] what Allah has given one over the other and what they spend [for maintenance] from their wealth. So righteous women are devoutly obedient, guarding in [the husband's] absence what Allah would have them guard. But those [wives] from whom you fear arrogance - [first] advise them; [then if they persist], forsake them in bed; and [finally], strike them. But if they obey you [once more], seek no means against them. Indeed, Allah is ever Exalted and Grand.
```
Q 2:282
```
And bring to witness two witnesses from among your men. And if there are not two men [available], then a man and two women from those whom you accept as witnesses – so that if one of the women errs, then the other can remind her.
```

Q 65:4
```
(As for) those of your women who have no hope of (further) menstruation: if you are in doubt, their waiting period is three months, and (also for) those who have not (yet) menstruated” [emphasis added].
```



- A Muslim would always choose a Muslim over a non-Muslim. Yes family ties are stronger there by force or by love.
