# Introduction

On 9th December 2019, Indian Government passed a Citizenship Amendment Bill, which is now an Act, effective 10th January 2020. The Citizenship Amendment Act (CAA) grants the 'Citizen of India' status to the refugees from Afghanistan, Pakistan and Bangladesh who fled the country due to religious violence and were accepted in India before 2014.

Hence refugees of all religion, except Islam, from the said countries gets the Citizenship of India if they have been living in the country for 6 or more years. Since, the act excludes Muslims (followers of Islam) from the list of accepted religious minorities, it has lead to many Muslim leaders and a lot of people of India to protest.

## Proponents of CAA:

  1. The neighbouring Islamic state of Afghanistan, Pakistan and Bangladesh had shown intolerance to non-Islamic groups in the past which lead to refugees (from religious minorities) fleeing to India. Those who have been living here for long should be granted legal status.

 2. Refugees were accepted by previous government without making any laws to grant them basic human rights. Now we ought to

 3. Muslims are not the religious minority in the neighbouring states under consideration and hence do not qualify for fleeing the country due to religious violence criteria. Those are refugees due to economic conditions and then India will have to consider accepting refugees from our friendly neighbour Nepal, Sri Lanka and Myanmar too.
  
 4. India should not accept economy refugee. Since, its still a developing nation and already struggling to support a rapidly growing nation of 1.3billion people.


## Opponents of CAA:
  
  1. The act is unconstitutional since it specifically excludes Muslims, which is the religious minority of India.
  2. Many citizens of India would loose their citizenship when National Register of Citizenship (NRC) will be implemented, since many poor/vulnerable groups would not have documents. In that case people of all religions except Islam will still remain citizens of India due to CAA, while Muslim citizens will loose their citizenship.
  3. Myanmar which is another neighbouring state has majority Buddhist population and many Muslims flee the country to escape religious violence and some of them did come to India. So why are we not making law for them too.
  4. Northeast India is majorly impacted where lots of refugees came from Bangladesh and if CAA grants citizenship to these refugees, this will create more competition in job market and burden the current education infrastructure.

## What bothers me about the protests?

 My opinion in this post is not to take a stance on weather this Act should or should not be implemented. Because there are many valid points to argue on each side.
 What I do want to oppose is the polarization of our nation as Pro-Islam and Anti-Islam.

 I recently read a book Sold: A story about a 13 year old girl from Nepal, smuggled illegally to India and forced into prostitution. This book talks about 1000s of young girls smuggled into India illegally every year lured as an escape to their current economic conditions. 
 
 I see when NRC is implemented this girl will be found illegally present in India and she will be sent back to her home country irrespective of her religion. Yes, a 'Hindu' staying in India is not guaranteed to get citizenship of India because of CAA. I'm happy for her and many more like her. The **NRC will help identify people smuggled illegally into India and send them back to their home country where they have access to basic human rights and legal resident status**.
 
At the same time **CAA will protect and bring justice to the ones who did not have access to basic human rights in their home country due to religious restrictions**. The people who were accepted by India 6years ago. The refugees who despite being lawfully accepted did not have the right to education or work in India until now.

 I see media while opposing or supporting CAA has been specially highlighting many speculative scenarios and just a single facet of a very complex problem and this is polarizing the country on this topic.

## Why Media choose religious facet

It is more beneficial for media to assume and accept the former explanation, because that leads to more engagement. If you and I agreed, then there would be nothing more to discuss no engagement. It gets more fun when you and I are on different sides and put our views out there. But -

An important thing to note is that, it is a sign of healthy nation when we don't agree on all the things and debate in public. But it is a sign of sick nation when the discussion deviates from the actual issue and we start putting up speculations instead of facts.

## Conclusion

**Is it really about Muslims?**

 In my opinion answer is **No!**

It is really about the perspective. We can choose to create a victim and demon mentality for a certain community and polarize our nation into extremes or we can see the positive side of people getting access to basic human rights.

While there are lots of pros and cons for the CAA, I do not agree we need to victimize and/or demonize any side.
