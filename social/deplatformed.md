# 'Deplatformed'- Violation of Free Speech?

Recent twitter account suspension of United States' CARTOON President - Donald Trump and removal of Parler from App stores and AWS servers has sparked a lot of outrage as well as applause for the Big Techs - Facebook, Apple, Google, Amazon and Twitter. Some believe this action violates Free Speech while others say businesses should be aloud to run as they wish. I agree with neither. In this post I will voice my concerns on the issue of 'Deplatform' and an urgent need for Censorship Resistent Information Distribution Platform. 

Request to the reader.... **Kindly forget about your hatred for DJT or any political leader for the duration of this article 😇**

## Does Social Media censorship violates right to Free Speach?
Technically **No!** As many have pointed out, even after being 'deplatformed' a user is free to voice his/her concerns and idea - via other information broadcast media such as paper pamphlets, radio service etc; move to some other platform; run their own blog and so on. In practice, it is more problematic than it sounds.

### What's the Problem?

- ***high EXIT cost***: Lost Audience! Lost Content!

    An individual does not have any easy way to move their entire audience to a new platform. The entire audience can't and won't just straight up download some new app. Think of it ... How would the suspended account will update all of their followers about the new *hangout* spot? And what about all the precious content the individual has already created and curated on the platform they are getting kicked out from?

    Currently most countries does not have any law to protect individual's right to have access to his/her own profile stored on these Tech Platforms. In most cases once an individual is booted-out from the system, they loose access to all the precious content on that platform.
    (India is one of the only country, I know of, that is working on Data Protection Architecture to allow individuals to own and benefit from their own digital data on tech platforms, but it is still in its planning phase and not implemented yet)

- ***monopoly of existing players***: Ban on App Stores! Shutdown Servers!

    As we saw in the case of Parler App. Google removed it from their app store, Apple sent them notice to update moderation policies i.e. *silence certain voices* or face removal and Amazon refuse to continue providing AWS webhosting service to Parler.
    
    There are very smart engineers, appointed to a very highly paid job to make sure only the apps 'THEY' think is right are allowed to run on the device 'YOU' bought from 'YOUR' hard earned money. ***Now, even if the entire audience coordinate to move to an alternative platform, can this alternative platform really exist in today's scenario?***

    What does one do in such situation? Create a whole new mobile devices that run on a whole new Operating System and start an all new server company to support these endeavour? Or maybe settle for traditional information media like newspaper and radio? Resort to the media that existed 100 years ago and the one that our law makers provisioned for free speech? I don't know about you but the idea of settling down for the media 1000x less popular and significantly slower than the Social Media not sound appealing to me.

- ***ACCEPTABLE speech laws set by non-elected oligarchs***: Who decided on these Moderation policies?

    Now this is something I have been personally supper annoyed with. We know Social Media amplifies the voice at a scale unimaginable before. Today's generation gets to know about not just the events of their home country but also about the world over current affairs from Social Media. Now what gets flagged as violent and improper is decided by some administrator officials of these tech platforms who nobody elected. These officials can at their discretion block whatever information that they think could do public harm. So what we see is a highly curated bubble of world events that are approved based on the ideologies and rules set by some tech officials that we did not even elect.
    
    Here I want to share about another event which was much more infuriating but got way less attention than the Cartoon Presided. This is what happened with *@ArticlesOfUnity* account on Twitter - which talked about creating a third party with centre political view with a conservative and liberal candidate running together. There was no mention of any violence or any misconduct that I could fathom. If you like you can do your own research [here](https://articlesofunity.org). Yet the account was permanently suspended. Why? Well we don't know because we did not vote on the moderation policy of Twitter. 
    
    Since we do not get to vote on these policies, we also do not get to choose which voices get amplified on the most popular 'Modern News Platform'


## Conclusion

I see Social Media as the modern and currently most popular way of consuming latest news. By not being censorship resistant it is promoting selective ideologies while silencing the voice of those with whom certain non elected individuals does not agree with.

All authoritarian regime starts with silencing the voice of those we don't like and is well accepted by the common enemies of the outcast. Just to put this in perspective - Nazi Party rose to power by the support of the people of Germany and in Nazi Germany all attempts by Jews to get attention to holocaust were seen as threat to public safety and were muzzled.

## Disclaimer

Censorship and Promotion of selected ideologies on popular social media platforms are common. Earlier I was pretty mad at Big Tech (Twitter, Facebook, YouTube etc.) for banning 'articles of unity' and some 'science journals'. But I do not believe CEO of Big Tech companies really have that much power. They are probably under lot of pressure to ban the 'EVIL' guys. What we really need is ***Censorship Resistant Information Distribution Platform***

Also, I hate viewing the side we do not agree with as simply evil. ***Do Not Hate The Player, Hate The Game***