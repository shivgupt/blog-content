# Inspiration

Often, I come across friends talking about their low fat or no fat diet and yet not being able to cut down on their extra fat. Many of them would see me eating bunch of cheese in most of my meals, food cooked in a good serving of Ghee or Butter, Coconut oil in my coffee and conclude that I have a gifted metabolism to be able to eat so much fat and yet maintain skinny waist. Clearly these people are concerned about their health and consciously making healthy eating choice and yet a lot of them suffer from over-weight, high (bad) cholesterol or blood pressure problems.

Most of the major cities of India has high numbers of over-weight/obese population: Chennai (39%), Hyderabad(34%), Kolkata(30%) and Delhi(29%) and this percentage has been rising even though more and more people are now days opting for a healthy diet. So is the diet a lot of these people are opting for really healthy?

Lately, I have seen there is a prevailing mindset that a high-fat diet is the reason for over-weight and cardio vascular problems. While we ignore the real danger - Sugar, lurking in the shadows.

In this post I will be discussing why in my opinion 'Sugar' is the major cause of the Obesity epidemic world wide. How excess of Sugar drinks are as bad for our liver as excess of alcohol. And how low-fat diet is not always the healthier option.

## Sources of Energy

Before we dig into the problem of Excess of calories (energy) lets look into the categories of energy source.

Carbohydrates, proteins and fats together supply 100% of the energy in our diet. While all three are mainly digested in intestine and provide energy, the energy density of each of these and the basic units into which they are broken for various body functions differs. 

- **Carbohydrates**
  - broken into Sugars
  - the quickest source of energy and quickly increase the blood sugar
  - required for immediate energy (such as for sprinting or before 🏋️)
  - body breaks the excess (unconsumed) carbohydrates and stores its components as fat.

- **Protein**
  - broken into amino acids
  - much slower and longer lasting source of energy compared to carbohydrates
  - needed by the body to maintain & replace tissues and to function & grow
  - body breaks the excess (unconsumed) protein and stores its components as fat.

- **Fats**
  - broken into fatty acids and glycerol
  - the slowest and the most efficient source of energy
  - required to synthesize hormones and other substances needed for the body’s activities

Hence, body requires a balanced diet comprising of all the sources and it converts excess/unused calories (energy) from any source (including protein and carbohydrates) into fats for future rainy-day use (which, unless you fast or unexpectedly get trapped on deserted island, rarely gets used).

### Lesson 1
**_Fat intake is not the only source of stored extra fat in our body. Body converts excess calories from protein and carbohydrates as well into fats to store in abdomen and under skin and in some cases in blood vessels and within organs._**

## Low-Carb vs Low-Fat

Low-Carb and Low-Fat diets are the two most commonly recommended diet tricks for weight loss.

  **Proponents of Low-Fat**: Fat is the most dense source of calorie. By cutting down on fat intake you cut down significant amount of calories and hence reduce the amount of excess (unused) calories that would be eventually stored as fat.

  **Proponents of Slow/Low-Carb**: High glycemic index food are quickest to digested, thus increased carbohydrates in diet can cause insulin resistance leading to metabolic syndrome which leads to increased waist circumference and high blood sugar. By reducing the amount of carbs or restricting the diet to carbs with low glycemic index (such as high fiber carbs like vegetables) can prevent overloading liver with sudden burst of sugar.

At first glance it seems:

  - Low-fat diet is a great option and maybe simpler to stick to if you are trying to cut down that belly fat.
  - Low-carb diet would be your choice in case you are suffering from type-2 diabetes.

But the problem is, this simple diet solution is not that simple because:
  
  - More often many low-fat products are also very high on Sugar. Remember the basics - salt, acid, fat and heat that gives taste to food. Well when you remove fat from the food, you generally end up taking more Sugar to cover up for the missing taste. We shall discuss the issue with Sugar in following sections
  - Fat is energy dense and when you reduce it significantly in your diet then what does it do to your hunger? Well you quickly digest the carbs for immediate energy, then the slower energy from proteins gets released which is used to repair your muscles and without the slowest energy release from the fats you get hungry much earlier than usual. What does hunger do to you? Of many things it affects your concentration levels. Also, since you are hungry more often, the chances of falling off from the diet plan becomes high. While on the other hand with high-fat and low-carb a person can eat until they are full and do not feel hungry until long and hence eat lesser calories.
  - Often with low-fat diets the HDL (good cholesterol) levels decrease. Which on the other hand increases in the case of low-carb diet.
  
### Lesson 2
**_While both low-fat and low-carb might be equally effective in loosing weight, it is difficult to maintain low-carb diet and often the food substitutes available would result in increased Sugar intake and lower HDL (good cholesterol) levels._**

**_We need carbs for quick energy and cutting down all the carbs from our diet is neither sustainable and mostly not even advisable. But increased carbohydrates in diet can cause insulin resistance and problems related to it._**

**_Fat has little immediate effect on blood glucose levels and this is a key reason why low-carb & high-fat diets tend to produce better blood glucose control._**

## Types of Carbs and their health impact

There are two types of Carbohydrates:
    
  - Simple carbohydrates: monosaccharides (glucose, fructose, galactose) or disaccharides (lactose, maltose, sucrose)
  - Complex carbohydrates: made of 3 or more simple carbohydrates
    
Mostly, Simple carbohydrates have a very high glycemic index, i.e. they get digested very quickly and spike the blood sugar levels. Complex carbohydrates have lower glycemic index compared to Simple Carbohydrates but higher than proteins and much higher than fats.
  
Carbohydrates comprise of three different nutrients – starches, sugar & dietary fibre 

  - Sugars: Single carbohydrate molecules such as glucose, fructose, sucrose, lactose etc. Can be simple or complex carbohydrate molecule.
  - Starch: Long chains of sugar glucose joined together
  - Fiber: Indigestible form of carbohydrate found mainly in plant foods.

Simple Sugars have higher glycemic index than Complex Sugars. Not all Sugars are equally bad and in my opinion sucrose - Table Sugar is the worst.

## What is Table Sugar? 

As [wikipedia](https://en.wikipedia.org/wiki/Sugar) says, "Sugar is the generic name for sweet-tasting, soluble carbohydrates ....". The table Sugar is combo of two types of carbohydrates - glucose (gives energy) & fructose (gives sweet taste). Most of our table Sugar comes from vegetable - sugarcane and sugar beet. Sugar added in sweet drinks and food is same as Sugar in fruits and vegetables, but in a much more concentrated form and separated from the dietary fiber. Table Sugar has one of the highest glycemic index.

So Table Sugar is specific Sugar molecule - sucrose which is a di-saccharide made of two mono-saccharides (glucose & fructose). A general term Sugar refer to single carbohydrate molecule - Simple or Complex.

### Lesson 3

**_The term Sugar does not refer only to the sweet tasting sucrose - Table sugar, but in general to the entire category of Carbohydrate nutrients which comes from single molecule of carbohydrate. Not all Sugars are equally bad. Complex sugars generally have higher glycemic index than simple sugars and thus better energy source._**

## Why is Table Sugar bad for us?
One of the problems we discussed earlier is increased Sugar levels in low-fat diet. To understand why Sugar is bad for us, we need to understand how it is metabolized in our body.

Table Sugar consists of equal part Glucose and Fructose. While Glucose is mainly digested in stomach and acts as a source of instant energy to the body, Fructose provides no benefit to the body and is treated as toxic. Hence, Sugar is 100% digested in Liver (similar to the alcohol). This overloads the liver cell's power generators (mitochondria). It is responsible for the generation of a lot of bad fat which gets stored in fat tissues or exported as free fatty acids. This is responsible for heart problems, sugar belly and insulin resistance. The fat that is not able to get out of liver results in Non-Alcoholic Fatty liver disease. Fructose digestion also produces uric acid which raises blood pressure and causes hypertension

### Why Sugar in fruits not big problem?
  Eating fruit is not bad since, it is accompanied with lots of fibres. Our liver is pretty strong and can easily handle the steady stream of Sugar. It takes much longer to eat a full 3feet sugarcane than eating 1tbs of sugar obtained from it. Also, fibre is pretty filling so it is less likely to overdose on Sugar while eating fruits.

As my favourite human says, _**Separating carbs from fiber is the greatest dietary sin!**_. And when we separate the fiber from fruits and veggies to extract sucrose (glucose + fructose) i.e. 'table Sugar' we are making a not so healthy choice.

Even though fruits are healthy source of nutrition, fruit juice without any pulp in large quantity is equally bad as alcohol for our liver.

To get this into picture consider 1 glass Apple juice vs 1 apple
  - **Apple juice** ~ 4 apple's worth of Sugar without fibre: fast intestine absorption and fructose delivered to our liver instantly
  
  VS

  - **Apple** ~ 1 apple's worth of Sugar with fibre: slow intestinal absorption and steady stream of fructose reaching liver. Also, eating 4 Apple takes much longer than drinking 4 Apple as juice.

With that said, during the induction phase of low-carb diet, it is advisable to cut down on high Sugar fruits to onset weight loss. We shall discuss how low-carb & high-fat diet leads to weight loss in other post.

### Digestion of Sugars
 Lets look into how some Carbohydrates rich food gets digested in our system.

#### How does Starch in potato or a slice of bread gets metabolized in body
  - Potato is 100% long chains of glucose (starch) which first gets broken into simple Sugar
  - Bread might have varying amount of added sugar and fibre but is mostly glucose - simple Sugar.
  - 20% of glucose hits liver and 80% gets metabolized in intestine
  - for digestion it needs to stimulate pancreas to make insulin
  - insulin stimulates the receptor which causes series of reaction and activate enzyme (Glucokinase) for digestion
  - Glucose digested is stored in liver as Glycogen, that acts as reserve tank for energy
  - What does not get digested in liver gets metabolized and is used to produce bunch of energy (in form ATP)
  - Not all this energy gets burnt and enters new fat making cycle
  - Liver does not want fat inside it so it converts it to something (VLDL) that is stored in Fat tissue
  - This is responsible for making us fat and causes heart disease
  
Now this is not that bad because only 20% of glucose made it to liver. So moderate amount of potato and bread in our diets is acceptable.

Lets compare this to how Sweet-drink consisting of added table Sugar gets metabolized.

#### How does Table Sugar digested in the body
  - Table Sugar consist of equal parts glucose (50%) + fructose (50%)
  - _glucose_ gets digested as described above 20% in liver and 80% in intestine.
  - almost 100% _fructose_ gets metabolized in liver as poison.
  - everything goes straight to liver, does not gets converted to glycogen and overloads liver machinery.
  - it is exported out of liver in form of fat which is stored in fat tissue and causes heart problems
  - this gives soda/sugar belly and also leads to insulin resistance and causes pancreas to work harder
  - some fat that liver is not able to export out just sits in the liver and causes non-alcoholic fatty liver disease
  - fructose results in even more new fat making due to formation of Xylulose during its digestion
  - it also results in formation of uric acid which raises blood pressure and causes hyper tension

Ahhh, that is pretty bad. Finally, lets see how Alcohol is metabolized to our body.

#### How is alcohol digested in the body
  - Most alcoholic drinks have Alcohol levels around 8%-15% (in light liquor like beer/wine) and 30%-50% (in hard liquor like whiskey/vodka)
  - only 10% alcohol gets digested in stomach and rest 90% gets processed as poison in liver
  - it overloads the liver machinery and enter new fat making cycle
  - this bad fat is what gives people bear belly
  - liver also export some of the fat out to muscles causing muscle insulin resistance
  - some fat that liver is not able to export out just sits in the liver and causes alcoholic fatty liver disease
  - excess also causes inflammation that results in insulin resistance and makes pancreas work harder to do its job

_Did you see the similarities in digestion of Alcohol and Table Sugar (Fructose in particular)?_

### Lesson 4
**_Not all Sugars are equally bad. Eating small quantities of bread or potato is acceptable source of energy but excess can lead to increased waist line and heart disease. On the other hand even small quantity of sucrose is relatively bad and can be hard on liver if taken without fiber. Table Sugar is something we should avoid (or atleast consume in very low quantity) weather we are proponent of low-carb or low-fat diet plans._**

## How much Sugar is bad?
The recommended Sugar intake for a healthy adult is no more than **25g and 38g for female and male respectively**. This figure includes (but not restricted to) the Sugar present in Fruits, Milk solids, slice of bread, Potato, condiments like Ketchup, your favourite 'boil & eat' Soup etc.

Comparing it to Alcohol, having some drinks on a night out with your friends is not going to cause you liver problems but having a large drink with every meal would do some serious damage.
Similarly having a piece of cake at a party would not be that bad, but donuts/cookies everyday with your morning cup of coffee/tea or eating those low-fat sweet yogurt with every dinner will.

### Chronic exposure of Sugar
  
I was surprised that many of the problems caused by the continuous intake of Sugar overtime are similar to that caused by Alcohol.

Here is the list: 

  1. Hypertension
  2. Heart disease
  3. High blood triglycerides
  4. Pancreatitis
  5. Obesity
  6. Fatty liver disease
  7. Fetal alcohol syndrome
  8. Addiction

**_Fun fact, according to a 2007 study, Researchers at the University of Bordeaux found Sugar is 8 times more addictive than Cocaine. Hence, another reason why all packaged food (including marketed low-fat healthy food) generally include generous quantity of added Sugar. Sugar enhances the flavour and taps into your addiction to get you hooked to the product. Your loss and seller's profit 🤷_**

#### Disclaimer
I'm not a nutrition expert, but a foodie who loves to experiment with food and her diet. I have in past and still do fluctuate a lot in my body statistics based on my diet with variance of 32-23-32 on slow-carb & high-fat diet to 34-32-34 on yummy carbohydrates rich diet. I have really experimented with low-fat diet because I love butter and coconut oil and any attempt to start that diet plan generally fails pretty soon.

### Some fun video to checkout

  - [Carbohydrates impact health](https://www.youtube.com/watch?v=wxzc_2c6GMg)
