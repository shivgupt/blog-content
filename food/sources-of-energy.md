## Source of Energy
Carbohydrates, proteins, and fats supply 90% of the dry weight of the diet and 100% of its energy. All three provide energy (calories), but the amount of energy per gram differs.

Carbohydrates, proteins, and fats are digested in the intestine, where they are broken down into their basic units:

    Carbohydrates into sugars

    Proteins into amino acids

    Fats into fatty acids and glycerol

Fats:
  - fats are needed to synthesize hormones and other substances needed for the body’s activities (such as prostaglandins).
  - Fats are the slowest source of energy but the most energy-efficient form of food
  - because fats are such an efficient form of energy, the body stores any excess energy as fat
  - 90 grams per day

  - Trans fats (trans fatty acids) are a different category of fat. They are man-made. Consuming trans fats may adversely affect cholesterol levels in the body and may contribute to the risk of atherosclerosis.


Protein:
  - The body needs protein to maintain and replace tissues and to function and grow.
  - they are a much slower and longer-lasting source of energy than carbohydrates.
  - If more protein is consumed than is needed, the body breaks the protein down and stores its components as fat.
  - Adults need to eat about 60 grams of protein per day

Carbohydrates:
  - Simple carbohydrates: Quickest source of energy. They quickly increase the level of blood sugar. Eg. Fruits, dairy products, honey, sugar etc
  - Complex carbohydrates: Needs to be broken into simple carbs, hence slower (but still faster than fat & protein). They also increase blood sugar levels more slowly and for longer. Eg. Starches and fibers, in wheat products (such as breads and pastas), root vegetables (such as potatoes and sweet potatoes).

  - carbohydrates that increase blood sugar levels quickly also increase insulin levels quickly. The increase in insulin may result in low blood sugar levels (hypoglycemia) and hunger, which tends to lead to consuming excess calories and gaining weight.

# Does Fat really makes you Fat?
Some low fat foods, such as yoghurts, contain significantly more sugar so being ‘low fat’ doesn’t necessarily make it healthier.
Unlike sugar, fat has little immediate effect on blood glucose levels and this a key reason why low carb, higher fat diets tend to produce better blood glucose control.

**--------------------------**

## The facebook poll
In October 2018, I conducted a facebook poll for what should be avoided for someone trying to loose wait and aiming for healthier heart.

Its true that the right choice would mostly differ from person to person when it comes to loosing weight. But for most the right choice would be Fruit Juice.

Fruit Juice = sugar without fibre
Butter/Ghee = fat

 Some of the people recognize that the real culprit is sugar (carbohydrates) but probably overlook its presence in lots of food items and that's why I choose 'Fruit Juice' to represent the sugar in 'Fat vs Sugar' poll.

 - Obesity/overweight is a big problem
