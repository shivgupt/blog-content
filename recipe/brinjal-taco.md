# Bringel Taco Recipe

## How to make

Egg plant, tomato salsa (or [pizza sauce](/almond-crust-pizza#pizza-sauce)) and bell-pepper

- Cut egg-plant in round slices and slightly salt them (about 15-20mins before baking).
- Brush olive oil on both sides and bake it for 35mins. Flip the egg plant half-way through baking.

Meanwhile, get the topping ready

  - Saute garlic and bell-pepper in olive oil
  - Add 1tbs red pizza sauce, salt to taste and black pepper to the saute mix.

Once the egg plant is baked add the topping and some cheese on the egg plant slices and bake it for 3-5mins more until cheese has melted a little.

Garnish with fresh coriander and serve warm.

![Yum 🤤](/ipfs/QmZzoowJNKrNLDR6cwZPf7Rm5wG4vCoqh76RGcyrPSjev4)
