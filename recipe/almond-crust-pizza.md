# Almond Crust Pizza 🍕

![Almond Crust Pizza](/ipfs/QmethvMGGA8Da3zfytrpzqKHDBwEa1wUW4exhKuWiyS7Cw)

This is one of my favourite healthy and tasty dinner where I substitute the traditional pizza base with a low GI crust made of almonds and cashews.

## Pizza base

### Ingredients for base

  - 100g Almond (variation: 50g cashew + 50g almond)
  - 🍝 Italian seasoning: Oregano, Thyme, Rosemary, Red pepper flake
  - 🧂 Salt
  - 🥚 2 egg (recommended but can skip if vegan)
  - 🥛 1/2 cup milk
  - Olive oil

### How to make

  - Grind the Almonds into flour in the blender/food-processor (with dry grind blade setting).
  - Add salt (to taste) and 1tsp of Italian seasoning to flour and mix well.
  - In a separate bowl mix the wet ingredients - 1/2 cup milk, 2 eggs and 1tbs of olive oil. If you are not using eggs, add a little bit more milk to get the mixture well blended.
  - Add wet ingredient mixture to the flour mix and blend it well using blender or by hand.
  - Grease the baking plate with some oil.
  - Pore the blended mixture on the baking plate and spread it evenly in the desired shape.
  - Pre-heat oven at 200℃ for 10 minutes
  - Bake the crust for 15-20 minutes or until lightly browned.

## Pizza Sauce

### Ingredients for sauce

  - 1 large onion
  - 🍅 3-4 medium size tomatoes
  - 5-6 garlic cloves (minced)
  - Italian seasoning: Oregano, Thyme, Rosemary, Red pepper flake
  - Olive oil

### How to make

  - Blanch tomatoes for 20-30 minutes. Peel the skin off and remove the seeds once cool.
  - Tip: If you don't mind tomato skin for some extra fiber, then you can just dice the tomatoes and saute them in pan with onions & garlic until it becomes mushy.
  - Add olive oil to pan and saute chopped onion and minced garlic for 2-3 minutes.
  - Add the blanched tomatoes (cut in halves), sautéed onions and garlic in a blender and blend it to a fine paste.
  - Pour the blended mixture in the pan. Add salt (to taste) and 1tbs Italian seasoning.
  - Cook the sauce over medium high heat (~10 minutes) and bring it to boil.
  - Simmer for few minutes and the sauce is ready.

## Pizza

### Ingredients for pizza

  - [Pizza Base](#pizza-base)
  - [Pizza Sauce](#pizza-sauce)
  - Grated Mozzarella cheese (or any other cheese of your choice)
  - Pizza toppings of your choice: My favourites are zucchini, mushroom and onion
  - Salt
  - Italian seasoning: Oregano, Thyme, Rosemary, Red pepper flake
  - Olive oil

### How to make

  - Add Olive to the pan and saute your toppings for 3-5 minutes.
  - Add salt to taste and 1tbs Italian seasoning to toppings and mix.
  - Layer the pizza base with the homemade pizza sauce.
  - Place the sautéed toppings on the layer of sauce.
  - Cover the toppings on your base with lots of cheese 😋
  - Bake your pizza for ~5-6 minutes and enjoy
