# Burrito

## Ingredients

### For Beans
- 2 cup rajma (Kidney beans)
- 2 medium onion
- Cilantro (can substitute with coriander)
- Cumin seeds
- 5-6 garlic cloves
- Dry coriander seeds
- Bay leaf (tez patta)
- 2-3  jalapeño peppers
- Choice of cheese ( I ❤️ Cheddar or Monterey jack or Asiago)
- [Chipotle peppers](https://www.amazon.in/Costena-Chipotle-Peppers-Adoba-Sauce/dp/B0014CVT6C/ref=sr_1_7?crid=1K4CPO73HCLC2&dchild=1&keywords=chipotle+sauce&qid=1619597063&sprefix=chipotle%2Caps%2C319&sr=8-7)

### For Pico de gallo (aka salsa)
- 1 medium onion
- 2-3 medium tomato
- 5-6 garlic cloves
- Cilantro leaves (can substitute with coriander)
- 2-3  jalapeño peppers
- 1 Lemon

### For Sour Cream
- 1/4 cup milk
- 3/4 cup heavy cream
- 2 lemon

### Tortilla
- Flour of choice: Whole wheat or refined wheat or corn
- Water

## Method

### Beans

- Rinse and soak rajma for 8hours. If forgot to soak them in advance, pre-cook them in pressure cooker for additional 45minutes.

- Add bay leaf and soaked rajma in pressure cooker with a pinch of salt. Add water till it is about 1/2 inch above the beans and pressure cook for 30minutes.

- Roast cumin and coriander seeds in a pan (1-2 mins) and grind them to powder.

- Finely chop onion, garlic and jalapeño peppers.

- Heat a pan and add some butter to it. Sauté garlic and onion.

- Add jalapeños peppers and chipotle peppers. Chipotle peppers are pretty spicy so add it according to your taste. Also, chipotle in adobo sauce have a slight smoky flavour which some people (like my hubby) love and some (like me) don't. So add according t your liking or skip all together to substitute with some other chilli. Cook for about 1minute more.

- Once beans are pressure cooked remove the excess water and keep it separate. Add ground cumin + coriander powder and the sauté mix to the beans. Add salt to taste and cook it open for few minutes while mashing most of the beans.

- Add 100g grated cheese to the beans and stir.
