# Mint Chutney

## Ingredients

- Cilantro/Coriander: 1 bunch
- Mint leaves: 4-5 leaves OR dry mint powder: 1/2 tsp
- Aamchur aka dried mango powder (1 tsp)
- (Optional, if not using Aamchur) Lemon juice: 1 tsp 
- Green chillies: 2 or to desired spice level
- Salt: 1tsp or to taste
- Dry coriander powder: 1/2 tbsp 

## How to make

- Wash coriander leaves and sticks well to get rid of soil
- put all ingredients in a blender and blend 😝
- can store in glass container for ~7-10days
