
# Ingredients

## Curry
- 2 cup yogurt
- 3-4 tbsp basan
- 2 cups water
- salt to taste
- 1/4 tsp tumeric
- 1/2 tsp dunhya powder (dry corriander powder)
- 1/2 tsp lal mirch powder (red chilli powder)

## Chonk
- ghee
- pinch of hing
- 1/4 tbsp mustard seeds
- 1/4 tbsp fenugreek
- 6-7 curry leaves
- diced red chili peppers to desired spice level

## Rice
- rice, duh
- water

=====

# Procedure

- taste yogurt, it's best when the yogurt is a hint sour. If it's not sour already you can add some dry mango powder
- mix all curry ingredients in a bowl, blend with hand blender until smooth
- heat ghee in crock pot or pan & add all Chonk ingredients and mix
- quickly add curry mixture to chonk pot plus an extra couple cups of water and cook on low for 30-40 minutes.
- cook rice when the curry is almost ready
- serve curry on top of rice with more ghee on top & enjoy

