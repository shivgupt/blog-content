# Malai-Kofta

'Kofta' is a generic name for a fried dumpling/ball made with minced veggies, paneer or meat while 'Malai' means cream in Hindi. Hence, 'Malai-Kofta' the creamy delicious fried veggie ball. It is one of my favourite dishes to make while hosting a fancy Indian dinner.

## Kofta: the vegetable dumpling
While Kofta can be made with many different mix of veggies according to your liking, this recipe is for the kofta in a very popular North Indian dinner entrée - Malai Kofta, often eaten with Indian breads naan or roti.

I have included the standard recipe here though, in order to avoid taking calories in form of starch (potato), I generally like to use not more than 1 potato and instead replace it with more paneer. But if it is going to be your cheat day dinner, then just follow as is 'cause potato is definitely very tasty 🤣

### Ingredients for 8-10 balls

  - 🥔 2-3 medium size potato
  - 🧀 200g paneer
  - 🍇 8-10 raisins (optional)
  - 🧂  salt to taste
  - 🌶 0.25 tbs chilli powder (optional)
  - 🥣 1tbs garam-masala
  - 🥣 1.5tbs ginger garlic paste (I like to use 50-50 ginger and garlic)
  - 🐤 4-5tbs chickpea flour (you can replace it with 2-3tbs corn starch)
  - 🔥 Oil for deep frying (I generally like to fry in Mustard or Sunflower oil)

### How to make
  - Boil potatoes until soft but not mushy. (In pressure cooker it generally takes 15minutes for 3 potato).
  - Once boiled potato cools down peel the skin off from the potatoes and crumble them in a mixing bowl.
  - Crumble the paneer and it to mixing bowl
  - Add all the dry spices to the mixing bowl
    - 1tbs garam masala,
    - 1.5tbs ginger garlic paste (add more garlic if you are a garlic lover 😋),
    - salt to taste,
    - 0.25 tbs chilli flakes (or less if you like less spicy food),
    - 4-5tbs chickpea flour/ 2-3tbs corn starch
  - Mix all the above well into a soft dough. Add more chickpea flour/corn starch if the binding feels loose and not holding well.
  - Divide the mix proportionately into 8-10 balls.
  - If using resin then flatten the ball and stuff a resin in middle before sealing it again.
  - Heat the oil for frying in a kadhai (a deep pot). One way of knowing the right temperature is by sliding a little bit of kofta mixture into the oil. It should float on oil without turning too brown right away.
  - Once the oil is hot enough set the flame to medium and slide 1 ball first to make sure it does not disintegrate/spill open in the oil.
  - If the ball disintegrates you would need to add more chickpea flour and/or corn starch. Else, continue to fry rest of the balls until they turn golden brown and crisp.
  - If cooking as snack: Serve it hot with condiments
  - If cooking for main Add kofta to [Malai sauce](#malai-kofta-sauce) just before serving with the bread of your choice (I like garlic naan)

## Malai Kofta sauce

### Ingredients for sauce
  - 0.5 cup cashews
  - 2 medium onion diced (optional, some people like pure tomato sauce)
  - 5 medium tomatoes diced (add 1-2 more if skipping onion)
  - 2tbs ginger garlic paste
  - 1tbs dry kasuri methi i.e. fenugreek leaves (optional, but recommended)
  - 1tbs dry coriander powder
  - 5 green cardamom pods (optional, you can skip it completely or add less than recommended. This gives a hint sweet taste to the food and I love cardamom so much that I always add couple extra than the suggested amount here)
  - 1tbs cinnamon powder
  - 2-3 cloves (optional, I often don't have it and skip since garam masala any way have cloves mixed in it)
  - 0.25 cup cream
  - 1tbs garam masala
  - Salt to taste
  - Chilli powder to taste (recommended 1tbs)
  - Cooking oil or butter (I generally cook in salted butter)

### How to make

  - Heat the oil or butter in a pan on medium flame.
  - If using Cardamom, remove the pods and crush/grind seeds into a fine powder.
  - If using Cinnamon stick, I recommend to grind it into fine powder too.
  - Add 1tbs cardamom and 1tbs cinnamon powder to hot oil and sauté for 30-45 seconds.
  - Add 1tbs ginger-garlic paste and saute for about a minute more.
  - Add diced onion and sauté until they caramelize and turn golden brown.
  - Add diced tomatoes and sauté until mushy.
  - Let the tomato and onion mix cool for a bit (5-10 mins) and then blend it into fine puree. You can substitute this step by using a puree of (instead of diced) onion and tomato. This way you can cook it all at once.
  - Cook the puree for about 4-5 mins.
  - Grind the cashews into fine powder and add it to mix. Cook for about a minute more
  - Add the following dry spices to puree and mix well
    - 1tbs garam masala
    - 1tbs coriander powder
    - Salt and Chilli powder to taste
    - A little bit sugar to taste (I avoid it most of the time, see my [Sugar post](/sugar-the-sweet-devil) to understand why sugar is dangerous)
  - Cook the mix for another 10 mins or until the raw smell goes away and mixture thickens a bit.
  - Add kasuri methi and cook for 30 seconds more.
  - Put it on low flame and add 0.25 cup cream. 
  - Stir gently and turn off the heat

## How to Serve
  - Add the Kofta (balls) to the sauce just before serving to avoid getting them all soggy.
  - Garlic Naan is my favourite bread to eat with Malai Kofta, though a whole wheat roti is a healthier and equally tasty option
