# Upma

## Ingredients

### First
- 1 teaspoon ghee
- 1 teaspoon mustard seeds
- 1 cup suji (semolina flour)
- a few curry leaves

### Second
- bell peppers
- onion
- red chili peppers to taste
- little bit of ghee
- pinch of haldi (turmeric)
- salt to taste
- pinch of chat masala
- lemon

## Procedure

- cook first batch of ingredients on low heat. Once it turns a hint golden, take it off the heat and set it aside
- saute second batch of ingredients until veggies start to get crispy
- add 3.5 cups water to veggie mixture and cook until water boils, then add some lemon juice
- add suji mixture to boiling veggies, stir for a minute and it's ready to serve
