# 7 Recipes for a vegetarian week

7 vegetarian dishes

- Almond & Cashew crust pizza
- Paneer butter masala
- Chickpea Salad
- Sautéed veggies with cheese toppings and your favourite sauce
- Paneer and cheese patty in lettuce wraps
- Burrito bowl without rice and paneer/tofu for protein
- Pesto and/or Alfredo zucchini bowl
- Baked brinjal with cheese and bell pepper stuffing with mushroom cream soup
