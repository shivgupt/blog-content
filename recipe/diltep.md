# Diltep - ranch like healthy salad dressing

## Make Tep
- 2 soft boiled egg
- 150g greek yoghurt
- 2tbsp lemon juice
- 2tbsp olive oil or whatever oil you like
- pinch of turmeric
- salt to taste
- 1 tsp pepper
- 1 tsp dill
- 1 tsp or to-taste garlic powder


Blend all of the above till fluffy and creamy. This will be our mayo equivalent with no raw eggs and less oil.


## Make Diltep
- 3/4 cup heavy whipping cream
- 1/4 cup [tep](#make-tep) (above blend)
- 2 tbsp whole milk
- 2 tbsp sour cream
- 2 tbsp lemon juice
- 1/2 tsp black pepper
- 1 tsp dill
- 1 tsp onion powder
- 1/2 tsp garlic powder
- 1 tbsp parsley flakes or dried chives
- salt to taste
