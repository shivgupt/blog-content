# Banana Bread

## Ingredients
- 3-4 ripe bananas
- 148g butter (lightly salted)
- 148g white flour (maida)
- 25g powdered sugar or half stevia + half sugar
- 1 tsp cinnamon
- 1 tsp vanilla extract
- 1tsp baking powder
- 2 Eggs (for eggless recipe add l milk instead)
 
## Method
 - If you don't have super ripe banana, then bake bananas in oven for 5mins on ~200°C

![image](/ipfs/QmZWsJYVyA4oKw7FjSNxSEKPsELUYwAH97ec455rJAWw8q)
*It should feel soft to touch and inside will be almost like pudding*

- Melt butter in the pan and mix sugar, cinnamon and vanilla extract

![image](/ipfs/QmUhymw6y5BPefYZVyQxfnRxqKytawQFmeq5zj2ckdF7jG)
*Swirl a bit so that the sides are greased*

- Add banana to the liquid mix and mash

![image](/ipfs/QmRXyV3VHmpcvfVUVbFgtU7tVcF2cJg9W2KNG47LcH5buu)
*Its ok to have banana chunks instead of smooth paste. I like chunks*

- Add eggs one at a time and mix

![image](/ipfs/Qmd7N9Wks6fZ4CmStiyW8CkJ6ebL57oSK4CQsZojGEf2qr)

- In seperate container make dry mix of flour and baking powder.  Then add dry mix to wet mix.

![image](/ipfs/QmQQjNXrudf8B7iHtBuXYZjdCNQQiqYh73m55CpkzxpsAT)
*Hand blend dry mix into wet mix*

- Bake for 50mins at 180°C

![image](/ipfs/QmcsVcFZAkXvcc4fuMPkYNUfnwVCBRu8GSAZy9bXP9gMgD)