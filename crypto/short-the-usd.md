# Short the USD

A DeFi tool on Ethereum blockchain to go long on leverage during bull market. This is not the best investment strategy but a simple Web3 project to self teach blockchain development with links to resources. By the end of this project you should be able to deploy a simple contract interacting with Aave protocol to deposit collateral (ETH, wBTC or any other supported crypto asset) in Aave lending pool, borrow USD stablecoin and buy more of the same or some other crypto asset, in other words short the $$ in favor of asset that you are bullish on.

## Setup

You can fork my [aave-playground](https://gitlab.com/shivgupt/aave-playground) repo for the setup or setup your own repo. Few useful tools/tech used here:

- [Hardhat](https://hardhat.org/getting-started/): for smart contract development environment
- [ethersjs](https://docs.ethers.io/v5/): for interacting with ethereum blockchain
- [Aave](https://docs.aave.com/portal/): Lending/Borrowing platform that we will use for leverage
- [The Graph](https://thegraph.com/explorer/subgraph/aave/protocol-v2?version=current): For quick access to indexed blockchain data
