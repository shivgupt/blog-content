Background: A lot of crypto lending/borrowing pools offer protocol governance tokens to users of protocols, which can be traded 💰🤩

'Yearn' is an automated service that allocates assets to various pools and maximise returns🛸

The strategy to allocate these assets is coded by community members 👩🏻‍💻👨🏻‍💻and selected by votes of $YFI holders.

Less than 1% fee is charged, 0.5% on withdraw + 5% on gas fee for harvest, to use this service. With about 70-80% ROI for some of the assets 🤯

The revenue collected via this fee goes to a treasury 💰, $500k is reserved for development and overflow is distributed to staked $YFI holders.

Since, its launch, 3 days ago, treasury has accumulated > $1m and overflow is already claimable by staking and voting in this system 💸🤑

Oh Yeah, $YFI ~ 3x $BTC in 2 months.

You can benefit from
