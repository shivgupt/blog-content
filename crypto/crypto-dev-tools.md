# Some helpful crypto dev tools

## [ethers.js](https://docs.ethers.io/v5/)

This library provides tools for interacting with Ethereum blockchain.

Example use:

```javascript
import { ethers } from "ethers";

let provider = ethers.providers.getDefaultProvider();

let txCount = provider.getTransactionCount("bohendo.eth").then(console.log);
```

Ethereum default provider points to Ethereum Mainnet. In above example we use query the tx count of Registered ENS - "bohendo.eth" from the blockchain.

One can use [Etherscan](https://etherscan.io/) to browse the Ethereum blockchain and verify results of simple query.

## [The Graph]("https://thegraph.com/explorer/")

This project indexes the blockchain data and makes it available via GraphQL.

One can use the hosted subgraphs on the explorer to query useful information in single request vs sending multiple request to blockchain.

Example use:

```javascript
import { axios } from "axios";

const uniSubgraphUrl = "https://api.thegraph.com/subgraphs/name/uniswap/uniswap-v2"

const query = `query{
 liquidityPositions (where: {user: "0xabcrdUSER-ADDress"}) {
   id
   pair {
     token0 { symbol }
     token1 { symbol }
   }
 }
}`

const sendQuery = async (q) => {
  const response = (await axios.post(
        uniSubgraphUrl,
        {
          q,
          variable: {},
          crossdomain: true,
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Content-Type": "json",
          }
        }
    ));
}

sendQuery(query)
```

In above example we query [uniswap subgraph](https://thegraph.com/explorer/subgraph/uniswap/uniswap-v2) to query liquidity-positions of the given user.

You can also run direct queries on the subgraph via the explorer [uniswap subgraph](https://thegraph.com/explorer/subgraph/uniswap/uniswap-v2) for testing and playing purpose.

Here is a nice [documentation](https://thegraph.com/docs/graphql-api#sorting) on how to form queries.
