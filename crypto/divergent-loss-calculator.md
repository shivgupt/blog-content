# How to calculate divergent loss

The following query gives the details of price ratios at the time of supplying liquidity and the current ratio of the said pair:

```
{
  liquidityPositionSnapshots (
    where:{
      user: "0xabc...",
    }
  ) {
    liquidityTokenBalance
    reserve0
    reserve1
    pair {
      token0Price
      totalSupply
      reserveUSD
    }
  }
}
```

supply-price = reserve0/reserve1

current-price = pair.token0Price

price_ratio = supply-price/current-price

divergence_loss = 2 * sqrt(price_ratio) / (1+price_ratio) — 1


The *divergence_loss* will gives the loss % realised by the supplier if the liquidity is withdrawn at this instant.



