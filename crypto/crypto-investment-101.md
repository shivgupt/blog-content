# Crypto Investment 101

**Crypto-Asset**  powered by *Decentralised Blockchain Tech* - is that hot chick in the bar whom everyone is interested in. Techie are optimistic about the tech, investors are interested in its potential market cap and speculators have lots to speculate here. This post will answer 
- ***Why Crypto?***
- ***What are Crypto Currencies?***
- ***How to invest in crypto?***

## Disclaimer
I'm not a financial advisor. I don't have any formal education in the field of finance and economics.

I'm a techie! A computer nerd who came across Blockchain during her Master's program and later quit Corporate American job to build on this revolutionary tech.  

## Why Crypto?

Growing distrust in the Governments and ever increasing globalization which demands trade in global currency not controlled by a single jurisdiction lead to the inception of Crypto currencies. Currencies which are censorship resistant and not controlled by anyone.

While the above ethos were enough for many nerds (including myself) to be gung-ho about the tech, it did not catch mainstream attention until recently. Some of the major noise was made with recent price spikes followed by **Tesla's $1.5 billion investment in Bitcoin**

But investment by big player is the effect of growing popularity of blockchain and not the cause of its popularity. 

### So ***why is this slow and not-completely ready tech gaining popularity ?***

Market movement depends on a lot of parallel stories, though if we zoom out enough, the entire crypto popularity aligns very well with the big debt cycle as described in [Changing World Order](https://www.principles.com/the-changing-world-order/).

#### Changing World Order: TLDR;
- Markets and economies follow long debt cycles which aligns with the rise and fall of various empires.

- A long-term debt cycle typically lasts about 50-100years. The current cycle began when the USA came to dominate the world trade i.e. about 75 years ago.

- The tell tail signs of a changing world order indicate the current long-term debt cycle is coming to an end
  - The USA is printing more and more USD to save the economy. **22% of all USD was created in 2020**
  - China, the contending superpower, already has risen to dominate the global trade share while the US shares have fallen by 14%.

  ![Global Trade](/ipfs/QmV8S4yx8CwwnESugHY4seLbpVr58mYEoT21GeGbhnoMP1)

- When over-printing is taken too far it leads to bank run dynamic, which in-turn prompts people to look for **alternate storeholds of wealth**

### My Take

While traditionally **alternate storeholds of wealth** have been 
  - hard money/asset such as gold or silver
  - currency of rising super-power which dominates the trades

Today, people also have new *tech* option ie. **crypto currency**. 
 - It is unstoppable ie. It cannot be banned by any Government
 - It gives power to the player ie. No one can take your money till you hold your private key safe.

## What are Crypto Currencies?

What is a currency? A store of value, used as medium of exchange. eg gold, silver etc.

What is **fiat currency**? A store of value which does not have any intrinsic value but is authorized, secured and backed by a government, who reserves the right to print more. eg USD, INR etc

What is **crypto currency**? A store of value which is used as medium of exchange but it is secured by cryptographic algorithms and backed by mutual trust in the system. eg BTC, ETH

Unlike 'fiat currency', 'crypto-currency' is maintained by distributed public ledger ie. a network of computers spread across the globe running the same software and validating/storing the same copies of database.

If today you want to transfer funds, in fiat currency, across the globe from the USA to India, you need to contact the firm that deals in foreign currency. They will take your money along with some service fee, calculated as a percent of total amount transferred, some foreign exchange fee and will then update their database as well as report to the central bank to update its database. The entire process would take about a week.

In case of crypto-currency, irrespective of recipient's location on the globe, you just enter amount to be transferred and submit online transaction with flat network fee (not dependent on amount being transferred). The transaction will take ~15 sec on Ethereum and ~10 minutes on Bitcoin. The entire process is completed within few minutes.

Crypto database is 'not centralized' rather it is distributed. No central bank of the US or India or any other country is involved in the transaction and the funds are secured by cryptographic algorithms. Until the time only you have access to your private key, no one can take your funds.

## Know Thy Tech

If you go to [CoinMarketCap](https://coinmarketcap.com/all/views/all/https://coinmarketcap.com/all/views/all/), you will notice there are over 9000 currencies listed. Obviously 90% of them are not worth putting your money in. We call them "shitcoins" in my line of trade.

Bitcoin (BTC) and Ethereum (ETH) are the two leading blockchain platforms.

Bitcoin is a simple blockchain which allows censorship resistant flow of value.

Ethereum is a more complex blockchain and is like a global computer on which multiple sophisticated apps (such as exchanges, lending and borrowing pools, etc) can be run. So Ethereum allows flow of value and also allows playing games, taking/giving crypto loans or swap one currency to another.

A lot of these Ethereum applications have their own 'protocol coin', which can be thought of similar to company stocks. These coins allow taking part in the protocols and also have speculative value akin to companies success.

So before investing in any crypto coin it is critical that one do their own research in the company's tech.

### My crypto "blue chips"

These are the tokens that I find interesting and believe to have some potential. **Do your own research too**

- ETH: Ethereum blockchain native coin
- BTC: Bitcoin blockchain native coin
- UNI: ERC20 on Ethereum blockchain. Protocol coin of Uniswap - a decentralized exchange on Ethereum.
- AAVE: ERC20 on Ethereum blockchain. Protocol coin of Aave - a decentralized lending/borrowing on Ethereum. 
- MKR: ERC20 on Ethereum blockchain. Protocol coin of Maker - a decentralized stablecoin on Ethereum. They created DAI stablecoin (1DAI = $1)
- GRT: ERC20 on Ethereum blockchain. Protocol coin of The Graph - an indexer which stores raw blockchain data in the form that can be consumed by web apps.

## How to get crypto?

The easiest way to get crypto is to buy from accredited centralized exchanges. Some of the exchanges that I have used:

- USA: Coinbase, Wyre
- India: Binance, WazirX

These exchanges would require you to KYC and then allow to buy crypto via credit card or bank account with a little exchange fee.

Once you buy crypto from an exchange it stays in your exchange account which is managed by the exchange on your behalf. Remember ***not your keys, not your coins*** ie. If you do not own the private key to the crypto account, then you do not have the above mentioned benefits of the decentralized currency. A private key is a set of '12 magic words' you generate randomly while creating a wallet.

When holding funds in an exchange your crypto currency is similar to stocks, from the traditional financial systems, maintained only in the books of the broker.

### Exchange holding vs Self holding

Exchange holding: When you hold your funds in an account created by exchange and do not have access to its private key

Pros:
- No network fee. Moving funds on a blockchain incurs 'network fee', which can be higher when the network is congested. Hence, when you sell or buy in a centralized exchange account you do not have to pay any extra network fee (which you would incur while moving funds on-chain or exchanging on decentralized exchange such as Uniswap)

Cons:
- No ownership of the funds. Anyone who has access to private key, also has access to the funds in that account. Hence, if the exchange gets hacked or shuts down, your funds are locked with the exchange itself.

Self holding: When you hold funds in a crypto wallet generated on your device and know the '12 magic words' aka private key. Some of the good crypto wallet apps out there are Metamask, Coinbase Wallet, Bitpay. Checkout [Operations Security](https://bohendo.com/opsec) to see how to secure your wallet.

Pros:
- You are in full control of your funds. If you don't pay taxes, government cannot cease your account and funds. If the wallet app you used is no longer on the app store, you can still use the same '12 magic words' to access funds from some other app on store or ask me to create a wallet app for you 😜

- No foreign exchange fee or high % fee on transfer. If you want to make payment in a foreign country (obviously subject to acceptance of crypto) or are travelling abroad and want to have their native cash, you just pay flat network fee irrespective of the amount and do the conversion at current rate without foreign exchange charges.

Cons:
- You are in full control of your funds. If you forget or compromise your '12 magic words' no one in this universe can help you retrieve your funds. People have lost millions because of their stupidity. So don't loose your key.

- High network fee. A flat network fee is incurred every time you move funds. So day trading and moving of small funds is rarely cost effective.

## Should I buy right now?

This is the most common question people ask and the answer is **I do not know**. It is true that current market cap of crypto assets is far below its potential. I do believe a single ETH in a decade's time might be worth 1000x of its current value. But will the entire crypto market crash tomorrow or day after? IDK. It is a volatile market and I do not do day trade. I'm involved with this technology for its potential to disrupt the system and bring revolutionary changes in long term.

So should you invest? Well if you are looking for some long-term investment, sure this is a great time because "Only few understand"....😝