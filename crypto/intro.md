# This is example code file for testing

```javascript
  import React from "react";
  import ReactDOM from "react-dom";
  import "./index.css";
  import App from "./App";
  import * as serviceWorker from "./serviceWorker";
  import { BrowserRouter as Router } from "react-router-dom";

  ReactDOM.render(
    <Router>
      <App />
    </Router>
    , document.getElementById("root"));

  // If you want your app to work offline and load faster, you can change
  // unregister() to register() below. Note this comes with some pitfalls.
  // Learn more about service workers: https://bit.ly/CRA-PWA
  serviceWorker.unregister();
```

```
  {
    "name": "blog",
    "registry": "registry.gitlab.com",
    "version": "0.3.0",
    "author": "Bo & Shiv Hendo",
    "license": "MIT",
    "devDependencies": {
      "@typescript-eslint/eslint-plugin": "4.14.2",
      "@typescript-eslint/parser": "4.14.2",
      "eslint": "7.19.0",
      "eslint-plugin-import": "2.22.1",
      "eslint-plugin-react": "7.22.0",
      "eslint-plugin-unused-imports": "1.0.3",
      "typescript": "4.1.3"
    }
  }
```
