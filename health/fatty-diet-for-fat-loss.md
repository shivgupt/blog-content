# Fatty diet for fat loss

In order to burn the excess fat in the body we need to kick-start the fat burning factory (FBF) of our body.

#### Note
- **Glycemic index <= 55 is considered low and healthy**.
- Refer this [Glycemic Index Chart](https://lowglycemiccertification.com/wp-content/uploads/2019/07/Food-Image.jpg). PS Whole Milk has lower GI than Fat free Milk.
- **10-20 calories does not break a fast, eg. some milk in tea or coffee**

## Phase 0

Week 1

So first week of this weight loss experiment we would have no explicit diet restriction. This week you shall eat as you usually do on any random week of the year and try to maintain a food-log. 
  - You should enter the time of your meals and a simple meal description eg. 2 eggs, 3 slice of pizza, 1 cup of lentils w 2 roti, 50g pasta etc. Don't worry if you don't know details of how many grams/cups of the dish you ate ... Just try to approximate.
  - Practice intermittent fasting. What is that? Try to squeeze all your meal in an 8hr window. eg. Your meal schedule might be like brunch around 1200, some mid-day snacks around 1500 and dinner by 1900.

You can skip intermittent fasting on 1 cheat-day of the week to go crazy and party only if you were strict the rest of the days.

## Phase 1

Week 2 - 4 (this may last for 2 extra weeks depending on weight loss progress)

Now you should continue with intermittent fasting with some dietary restrictions. Try eating foods with G.I. <= 30. While G.I. Less than 55 is considered low, we will focus on eating foods with super low G.I. This is because we want to switch are body from absorptive state to post absorptive state quickly during out intermittent fast. You can pick one day of the week as cheat day where you do not follow this rule and indulge in your favourite dishes/fruits/sweets. I prefer Friday or Saturday to be the cheat day, because these are the days I generally go out or see friends.
  
Food to avoid during this time except on cheat day.
  - All sweets - cake, cookies, sweet chocolate
  - Sweet fruits like apple, strawberries, pineapple, watermelon
  - High GI veggies - Potato, carrots
  - Rice, pasta, bread
  - Not-so healthy fats - soybean oil, cottonseed oil, canola oil, corn oil
  - Large quantity of Fried food

Food advisable to eat
  - Fiber rich veggies - broccoli, spinach, onion, mushrooms, lettuce
  - Nuts
  - Low GI fruits - avocado, tomato, brinjal
  - Cheese, paneer, tofu, meat
  - Full fat yoghurt
  - Healthy fats - virgin olive oil, virgin coconut oil, avocado oil, butter
  - Medium servings of Legumes

During this induction phase, try to eat diet consisting of slow to digest carbs and high protein and healthy fat.

## Phase 2

Week 5 and later

You can at this point add healthy carbs back to your diet. During this phase you should still avoid fruit juice (which is carbs without fiber) but you can start eating previously exclude fruits like apple, orange, berries. Rice and bread in small servings i.e. a medium bowl of rice or 2-3 slices of bread is also good. But do not increase your carbohydrate intake by a lot and all at once. Work your way up slowly and check your weight regularly.

During this phase it is also advisable to do 1 long fast once in 2 or 4 weeks, say every-first Monday of the month. While during rest of the week you will squeeze all your meals within a 4 or 8 hour window, on the long fast day you will not consume any calories. Fast is a healthy way of detoxing your body and tricking it into post absorptive state i.e. where it starts burning the stored fat to continue functioning.

### How to do one-day fast
Say you decided to fast on Monday. You shall eat your regular normal meal on Sunday as per your intermittent fast schedule. On Monday you shall consume no or negligible amount of calories. So some splash of milk in your tea and coffee are fine, but cookies or sugar with the tea/coffee is not. You should take no alcohol, fruit juice or nuts or snacks throughout the day. The day after the fast day, Tuesday in this case, you shall eat according to your normal diet plan and within the intermittent fasting window. So if on Sunday your meals were between 1300 - 1700 hrs, then you skip calories on Monday and on Tuesday your meal should be between 1300 - 1700 hrs.

Foods items advisable to open the fast
- nuts
- leafy green salad
- not so sugary soup

## Phase 3
_By this time you have reached or are about to reach your weight goals._

*_Maintain!_* the balanced diet and occasional fasts for detox. Don't fall back to your old eating habits.
