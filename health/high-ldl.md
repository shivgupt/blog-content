# Is high LDL Cholesterol necessarily bad? 

While talking about Cholesterol, we usually focus on two types

1. LDL (aka 👿 bad) cholesterol
2. HDL (aka 😇 good) cholesterol

Often, elevated LDL levels are related to risk of heart diseases irrespective of diet.

In this post I will discuss what causes elevated LDL and how to better interpret your lipid profile aka Cholesterol report.

## Background

On 22, Feb 2021, I got my Lipid profile report, according to which my LDL 👿 level is higher than optimal. Initially, as a naive non-medical professional I was very disturbed (and surprised) by the reference interpretation provided by the lab.

![image](/ipfs/QmaQ8hMFqhA8BNjV2wzZfsNenHHk5gBngXFqyNdC26v7tM)

> Total Cholesterol = HDL + LDL + 0.2 x Triglyceride

> Non-HDL Cholesterol = LDL + 0.2 x Triglyceride

It was a surprise because lately I have been living a pretty active lifestyle in the mountains, have been eating (what I consider) healthy and have been feeling fitter and stronger than anytime in the past 5-6 years. At this time, I'm in my late 20's, my body fat is ~18.5% and BMI ~19.

![image](/ipfs/QmTVvXxScpYXR8b53KppVUdtLHut5Ed4B7RJeMU68UPTRG)

So I did some research on **Cholesterol levels and what was going on?**

## Glossery Please...🧐

- **Triglyceride**: a common type of fat in body and *major source of energy on Low Carb diet*. It is not cholesterol, but still measured because elevated triglyceride levels increase the risk of fatty plaque buildup on artery walls.

- **VLDL**: _Very Low Density Lipoprotein_ molecules that carry *cholesterol*, *triglyceride* and some *fat soluble vitamins* from the liver to rest of the body for supplying energy. 

- **LDL**: _Low Density Lipoprotein_ molecules which are formed by breaking VLDL into *triglycerides* + LDL.

- **HDL**: _High Density Lipoprotein_ molecules that carry the unused cholesterol and triglycerides back to liver for recycling.

## Role of Lipoproteins, Cholesterol and Triglycerides

Cholesterol & Triglycerides, types of fat, are not soluble in water and thus can't swim around in our bloodstream to reach their destination. Hence, our body creates tiny ships aka VLDL to carry them. After offloading triglycerides to their destination, now these ships have less cargo and are called LDL. These ships then deliver cholesterol to the cells for their repair work. Unused LDL hangout in blood waiting for HDL to take them back. HDL is a mostly empty ship which picks up all the unused cholesterol, triglycerides and accumulated plaque in the blood vessels and takes them back to the liver. The liver recycles the unused cholesterol and uses it for producing enzymes such as bile for digestion.

In case of low HDL levels all the unused cholesterol and triglycerides are not transported back to the liver. In such scenario VLDL & LDL gets oxidized to form plaque that sticks to the side of the blood vessels and leads to Cardio Vascular Diseases. Hence, the common interpretation HDL - 😇 good cholesterol and LDL - 👿 bad cholesterol.

![image](/ipfs/QmVU96QDvUo8Ukz3iDUqHddJ5Fw2GLM5RGpv8k2wHWSekW)

## What causes a spike in LDL levels in the bloodstream?

Naturally my first question when I first saw my lipid panel was: **What does my high LDL level even mean?**

Here are some situations that unusually spike the LDL and VLDL levels:

  - **Multi-day fast, weight loss or low carb + high fat diet**
  
    **Triglycerides** are the major source of energy if the body is ***in weight loss phase and/or burning fat*** for providing energy. The liver meets the body's energy requirement by producing more VLDL which in turn leaves behind higher amounts of LDL once the Triglycerides are delivered to the cell.

  - **Cholesterol is needed for the production of stress hormone**
  
    Our body uses Cortisol (stress hormone) to maintain blood pressure, immune function, manage stress etc. A bad night sleep or a caffeinated drink or a sudden stress, all can result in increased cortisol level. Which in turn leads to the liver producing more cholesterol and thus spike LDL levels.
  
  - **Low fat content in the diet**
    
    Cholesterol is required by the body to make vitamin D, hormones (such as sex and stress hormones) and bile acids for digestion. Hence, the liver works hard to maintain cholesterol levels. While Chylomicrons are the fat from food, LDL is the fat produced in our body. When there is not enough fat in the diet, liver produces more fat to make up for it.

### Why was my LDL high?

I ❤️ coffee and have a cup of strong coffee with 2 tbps cream every morning. I was told by the lab not to have any heavy meal but a cup of coffee or tea is acceptable. Also, I have been on a mostly low/slow carbohydrate and high fat diet for ~5 years.

## Better way of interpreting Lipid Profile

So back to the original question, **is high LDL necessarily bad?**

Before we conclude I would like to share two interesting results from a [study from 2001](https://jamanetwork.com/journals/jamainternalmedicine/fullarticle/647239), that came across during my research:

1. In the first case two population sets, Low LDL and High LDL levels, were taken and their risk of heart disease (i.e. IHD) was compared based on their Triglyceride and HDL levels

![image](/ipfs/Qmdy45FKmAL18jsdwt9mTuyrkVcC3KwixcjbCA4nsB1qhe)

**The population with high LDL levels but low Triglyceride + high HDL is at lesser risk than the population with low LDL but high Triglyceride + low HDL.**

2. In the second case two population sets, Smoking and Non-smoking, were taken and their risk of heart disease (i.e. IHD) was compared based on their Triglyceride and HDL levels

![image](/ipfs/QmRU8V89hLtZqq8aCqjTtoCakGqkoBrPtTxdqcW1CQNaP9)

**The smoking population with low Triglyceride + high HDL is at lesser risk than the non-smoking population with high Triglyceride + low HDL.**

In both cases irrespective of LDL levels, _**the risk of heart disease is much higher in the population with High Triglyceride and Low HDL.**_ 

### So why does conventional study suggest LDL is bad?

While part of the reason is medical science is ever evolving and earlier we just did not understand our body as well as we do now. The other part of the reason is, Statins (used for lowering LDL) are one of the most profitable drugs in the market and the pharmaceutical giants have high influence on what gets taught in the medical school. This is well described by [The Oxidized Lipid Hypothesis](https://bohendo.com/ldl#the-oxidized-lipid-hypothesis).

## Conclusion

- For accurate results it is important to have proper testing condition eg 12-14 hour water only fast. Tea, coffee and caffeinated energy drinks can spike the LDL and hence Total Cholesterol levels.

- Since, weight loss as well as low carb diet can shoot up the LDL levels in-order to meet our body's energy requirements, measuring LDL alone does not provide the correct picture. Rather Low Triglyceride and High HDL are the better marker for good cholesterol irrespective of LDL. This is very well illustrated by [Cholesterol Code](https://cholesterolcode.com/a-simple-guide-to-cholesterol-on-low-carb-part-i/)

- My Triglyceride levels are pretty low and HDL levels are much higher. Thus, irrespective of my high total and LDL cholesterol, I feel safe interpreting this as pretty good cholesterol levels and low heart disease risk.
