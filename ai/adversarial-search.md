# Adversarial Search

When playing in a multi-agent environment which requires planning ahead while other players are planning against us

In this tutorial we will look at a variant of n-puzzle problem (let's take n = 8).

## Problem

Consider two 8-puzzle problems plaid by 2 players with following rules:

  - Two players alternately make a move on one of the puzzle.
  - Each turn they flip a coin to decide on which puzzle the move will be made.
  - First one to solve the puzzle wins the game.

## State space

  This is the list of all possible states the puzzle can be. So there are 9!

