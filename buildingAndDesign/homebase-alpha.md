# Homebase Alpha

Bo and I had been living a nomadic life since June 2018, hopping from one city to another and staying in airbnbs until the infamous 2020 lockdown. This is the journal and some snapshots of how we got our mountain home ready during the pandemic.

![image](/ipfs/QmWXrmkXdPT4mVnQQT53iHK7EebVFThrGiv1kg6XXqEwUM)

## Apartment Hunt (March 10 - 21)

We arrived at Mcleodganj, Dharamshala, on 10 March after a vacation and Bo's birthday trip in Shimla. We had booked an airbnb for 3 weeks but figured, due to Covid-19, travel won't be safe for a while. We started hunting for a place to stay for the rest of the year.

One week before lockdown, while chilling at [Illiterati cafe](https://goo.gl/maps/H8LhUmcDawdvbiUQ6) just below our airbnb, we met Ken who told us about 2 studios available in his building and got us in touch with the owner. It was in same area as the airbnb we were staying at. The location is very peaceful and a comfortable 15min walk away from the market. We decided to take both studios - one as work/office space and the other as bedroom.

We managed to get a bed and few kitchen supplies just a day before the lockdown but the place was nowhere near ready to live. So we continued staying in the airbnb until June.

![Blue room Original](/ipfs/QmZQFSiGbtYemRmRwa9en1H2DEkiS3jsHcZ2CDbH8NvosM)
*Studio 1, before remodelling.*

![Red room Original](/ipfs/QmV7WCZY5r5AjxpJBfoZtEAwpmXbwbNAipjAx7idGnhJ2W)
*Studio 2, before remodelling.*

## Apartment Designing (March 22 - May 6)
During lockdown we fell in love with Mcleodganj and decided to remodel the studios and make into a small holiday home. We negotiated with the owner for a long term lease and permission to **remodel the 2 studios into a single 1BHK apartment**. 

![Apartment 3D model](/ipfs/QmaWbxoB9Vq7mDYpdXynuS3veR4MmPzU43i8TDm6r59jYQ)
*I spent all of April learning how to use [Blender](https://www.blender.org/) and created this 3D model of the apartment. Which proved to be helpful while explaining to the construction workers what we wanted.*

## Apartment Remodelling (May 7 - June 6)
In May, Himachal government allowed the construction work to resume in the city and soon we got 3 walls and a kitchen removed. It took about 1 month to get the construction completed, the rooms painted and the custom furniture made. We moved into our **Homebase Alpha** on 7 June, 2020.

The rest of this post is a detailed breakdown of work done and money spent to remodel and setup our apartment.

### Renovation Cost

> Total Construction + Paint Cost: ₹1,08,000 ~ $1450

Our focus was to do minimum construction and turn:

1. The existing kitchen in first studio into a study nook

![Nook side](/ipfs/QmbLUA4RBeHbapb2Ez3Mki1roHsPRarFyP1iFTqA3ZYfj9)
*Study nook with a large glass window in place of the kitchen in Studio 1*

2. Connect the two studios from inside

![Connecting Door](/ipfs/QmQ53MSJ6ptuw7MKzJMTAKzoa4Xw1sQubrLHP7Rfc64yyH)
*The wall replaced by door to move in between two rooms.*

3. Have a bigger balcony by joining the two

![Balcony](/ipfs/QmS4ercoV2EoB62zyfcXDd6TtayQTYN435boxQvXRGR9XG)
*We took down the wall separating two balconies. We also got power backup installed. You can see the little cabinet for an inverter and 2 batteries in the back.*

#### Construction material

> Construction material cost: ₹6000 ~ $80

We got a truck of sand and 2 bags of cement, this was 1/4th of what contractor had suggested us. Out of curiosity, I googled sand to cement mixing ratio and their density, took measurement to find area that needed to be smoothened and solved the math equation. To my surprise contractor had asked for 5x more than what was needed. I don't know whether he gave us the wrong estimate because his math is weak or if he planned to cheat us.

Though, I bought much less material than originally suggested, about half of the sand and 1 full bag of cement was left unused. Anyway we could not have purchased less than what we did because of the minimum quantity limit set by the seller 🤷🏼‍♀

We also got few tiles to cover-up/fix the kitchen area.

#### Paints 

> Paint cost: ₹16000 ~ $215

As you can see our apartment is a bit colorful and that is why we ended up with multiple small containers. We have about half filled containers of all the paints still left 😜

#### Breaking Walls, Kitchen and Finishing

> Construction labour cost: ₹15k ~ $200

Major construction work was done in *Studio 1*, where the kitchen was removed, all the underlying water pipes were blocked and a hole was made in the East facing wall. The wall separating two studios was also knocked down to connect the studios from inside. Outside the apartment we knocked down another wall for bigger balcony.

![Constructions](/ipfs/QmfWcQ2eHhwWwvPdDWRehq3WCy74Z5K4rrKANbKDdXFGSf)
*Took down the kitchen and the East facing wall for bigger window.*

#### Painter

> Paint labour cost: ₹27k ~ $362

We were later told by our neighbors that we overpaid about 2x the market rate for painter 🤦🏽‍♀️

#### Connecting Door

> Wooden door + installation cost: ₹22k ~ $295

The inside wall was replaced by a wooden door connecting *Blue room (Studio 1)* to *Red room (Studio 2)*. We opted for medium range local Himalayan wood - ***Kail***. Some of the other options in wood and particle-board were:

- Cheaper option: *₹10k - ₹17k ~ $135 - $230*
  
  Particle board or cheaper pine wood (our carpenter called it 'Cheel')

- Expensive option: *₹50,000 ~ $670* and above
  
  Teak, Deodar and Rosewood are all very beautiful wood with long life.

#### Big Window

> Big window glass _ installation cost: ₹22k ~ $295

![Nook view](/ipfs/QmTEGbE8GJJiYUUwXWJjryrKPK5VcgUncJCJy6kV1jekJu)
*Bo and I enjoy solving math equations and managing our chore chart on this big window with beautiful mountain view*

We again opted for medium range *10 mm* toughened glass for our window. Below are the alternate options that are available and our reason for not choosing them.

- Cheaper option: *₹6000 - ₹9000 ~ $80 $120*

  Normal glass (not toughened) is available in only standard sizes. This price estimate is of two normal glass of thickness *10mm or 12 mm* along with a wooden window frame.

  The cheaper option did not fit our need because the size of the largest glass piece available in *non toughened glass* was smaller than our window and we did not a two panel window with wooden frame.

- Expensive option: *₹84k ~ $1130*

  On higher end *vacuum glazed* windows are a great option especially in super hot or cold climate. The vacuum glaze provides insulation and reduces the cost of cooling or heating the place respectively.

  We did not go for expensive option because it was not available locally and lockdown restrictions made it uncertain when could we get it. Also, all the other windows in our apartment have normal glass so glazing just one window would not have helped with insulation significantly.

### Furniture Cost

> Total furniture cost: ₹2,68,000 ~ $3600

We spent about a day browsing through furniture shops in local Kangra market ~30km from our village and got our Bed and Dinning table there. All the other furniture was custom made by our local carpenters - Sushil and Narendar.

#### Study Nook

> Total of all study nook furniture: ₹56k ~ $750

- **Table + 2 chair**: *₹15k ~ $200*

- **Armoire**: *₹35k ~$470*

![Armoire 3D model](/ipfs/QmZTJWAGkE9Qow5hNMC9EnptQ3ePFfb5uXoJGtA8CS4XwE)
*3D model for armoire given to the carpenters to have it custom made.*

- **Divider Shelves**: *₹6000 ~ $80*

![Nook](/ipfs/Qmb2RR1XrgYrAgpQVC8HFEiSbTSh99xiAaY5hhk9vDVYBh)
*Re-used the marble salvaged from the torn down kitchen as the table top. I use this table as my giant mixing palette while painting. It is so much easier to cleanup acrylics on marble (with alcohol spray) compared to wooded/plastic ones.*

#### Bedroom

> Total of all bedroom furniture: ₹58k ~ $775
  
- **Bed**: *₹28k ~ $375*

- **Mattress**: *₹30k ~ $400*

![Blue room full](/ipfs/QmPV9wmUBnY443XYHTUfVLe351H9ig2c1ghBVBJ7rjgqw1)
*Studio 1 aka Blue Room. We got gym rings and yoga mats in our bedroom, motivating us to workout most days 😜*

#### Kitchen

> Total of all kitchen furniture: 58k ~ $780

- **Shelves**: *₹19k ~ $255*

  We did not like the existing kitchen cabinets but did not want to redo the entire kitchen. Now we use the cabinets as storage space for cleaning supplies and cooking gas cylinders. New addition to this kitchen includes a spice rack, 2 dry good racks and 2 dish racks.

![Kitchen](/ipfs/QmZqBEbWmqG2MQsF2XxdgAFrXL2EcrR9Bz2mYv6Q4hSZeR)
*Kitchen with extra shelves for dishes, dry goods and spices.*

![Dish rack](/ipfs/QmRPNX44HyXSaAnCXKGKvZHZUJGaWe3pB6Fx1J41zyZigr)
*Dish rack, Map of computer science and Kali painting by me*

- **Dinning Table**: *42k ~ $565*

  We took a 6 seater table with 4 chairs from Godrej. The company provided a hassle free delivery and assembly. Bo prefer having glass top worktable where its easy to scribble with our markers while working and brainstorming. We often leave notes for ourselves on the table as reminders.

![Kitchen](/ipfs/QmYLUzQGYZuU3ADatKias3Uxh8rmrYFrXHKDF5rDoUvtou)
*Studio 2 aka Red Room. Dinner + Bo's work table.*

#### Lounge

> Total of all lounge furniture: 81k ~ $1100

- **Sofa**: *₹75k ~ $1010*

  ![Couch 3D model](/ipfs/QmWwT29CVh58uDE9EC7ebLspRfscTy6q3CFZgCgv3RD3vm)
*We wanted an L-shaped couch which can also be turned into a double bed so we gave custom 3D model to our carpenter who did great job.*

  We splurged a little extra on our fancy couch. By the time sofa cushions were getting cut, the lockdown restrictions had eased out and we were able to get the fabric from Surat, Gujrat.

  - Cheaper option: *₹30k ~400*
    
    A much simpler pull-out couch with local Himachal upholstery and cheaper wood can be made in less than half the cost 

  - Expensive option: *1.5 lk ~ $2000 and above*
    
    We got estimates from our carpenter for the same design made with Sheesham or Deodar. These woods are more durable and in darker color. But we decided to go with the same wood that we used for rest of the furniture ie Kail. 

![Couch](/ipfs/QmVCrqnMg9Zo3FSEVPfpaNbpE5DJ9LgnrwHYekqczxsWko)
*Mid way through converting sofa into bed we realized it can stay in half-open configuration and we can have a built-in center table.*
  
- **Shoe stand**: *₹4500 ~$60*
    
    Its nice to have our footwear organized in a place and also have our keys, masks and umbrella ready for us by the door.

![Shoe-stand and Fridge](/ipfs/QmdGAR19LBmaG7ehHw429tCupCJYxS6UxUg4NzrBqfABuy)
*Shoe rack and another dry good shelf*

- **Book and Plant shelf**: *₹2000 ~ $30*

![Couch and Table](/ipfs/QmVL9PM74tfRsuwkMnxygQcByg2wATFwK7GVWRTn8um2He)
*Its nice to have shelf space just by our couch for the plants and books. Also, look how well the dehumidifier fits under the dinning table.*

#### Balcony

> Total balcony furniture: 15k ~ $205

- **Set of Porch Chairs**: *₹10k ~ $135*

    Bo and I love sitting in the balcony sipping tea.

- **Cabinet for Inverter and Batteries**: *₹5000 ~ $70*


### Home Goods and Decorations Cost

> Total: ₹3,24,000 ~ $4345

Since we had been living as nomads for ~2 years, we did not have even the basic home stuff like bedsheets and cups. Here is what all basics we had to buy to settle down comfortably.

- **Washing Machine**: *₹75k ~ $1000*

![Red-room Washroom](/ipfs/QmTnCinx3ysTfnebtQfEaWr7xwDAEDgpeWfRdjwhKHpKJw)
*Studio 1 bathroom turned into laundry + cleaning supply room. Winter and Monsoon in Dharamshala can get brutal and air drying clothes can take long. Hence, we got a dry + wash combo machine. Ohh I had missed the dryers from the USA, No more!*

- **Carpets**: *₹38k ~ $510*

  We have 2 carpets in each room. Bo and I had a bet before we went carpet shopping about how much it would cost. Bo's estimate was $4000 and mine was $500 for 4 carpets. At the time we did not know about the difference between hand vs machine made. We started by exploring some local emporiums which had sheep wool hand woven carpets. It almost felt like Bo won that bet, but we still had not found anything that we liked enough. We later came across a *home decor* shop - [Spaces](https://g.page/SPACES-Inside-Dharamshala?share) and that was it. We both were so happy to find something in fun modern-design prints. Alas! I won the bet.
  
  We enquired why some of those carpets were cheaper than the ones in traditional prints. Turns out what we selected were machine made carpets thus, ~8x cheaper 😄

  - Expensive alternative: *₹3 lk ~ $4025*

    Hand woven sheep wool carpet. Some carpet connoisseur might prefer this but we kids prefer machine made cheaper stuff 🤷🏼‍♀️

- **Kitchen Basics**: ₹*70k ~ $940*

  The above budget includes a medium size fridge, 2 burner gas stove, cooking pots and pans, 4 person dinner set, cutlery, mugs, borosilicate storage containers, kitchen gadgets like blender, toaster, electric kettle, knives, mini oven etc.

- **Gym Equipment**: ₹*10k ~ $135*

  Bo and I workout 3-4days a week regularly. For our home-gym we got gymnastic rings, pull-up bar, ab wheel, yoga mats, grip strengthener and push-up/L-sit bars.

- **Linens, Blankets and Pillows**: ₹*15k ~ $200*

- **Bathroom Basics**: ₹*5k ~ $70*

- **Lamps, Curtains and Blinds**: *₹21k ~ $280*

- **Backup Battery and Inverter setup**: *₹44k ~ $590*

  Generally we do not have power cuts, but every once in a while we have 5-6 hour down time. We did not want our wifi router to go down or laptop to go out of juice at that time. So we got a power backup setup. Its a 1500 kVA inverter that run on 2 batteries.

- **Miscellaneous Supplies**: *₹41k ~ $550*

  This includes electronics such as vacuum cleaner, power extension chords and dehumidifier. Also, some other basics like mop, broom stick and trash cans.

- Garden supply, Plants, Pots and Soil: *₹5k ~$70*

### Cost Comparison wrt Airbnb Surfing

During past 15months we stayed in a lot of airbnbs in India, Germany, Portugal, Japan, Czech and Netherlands. No doubt the rate of airbnb was least expensive in India and Czech. Per month average of some cheap options were around ₹45k ~ $600 and more expensive and fancy ones around ₹2 lk ~ $2000.

One year cost of airbnb surfing in India would be $7200 on lower end and $24k on higher end. **Considering we would have stayed in a nice airbnb most of the time, our housing expense for the whole year can be estimated around ₹12 lk ~ $16k**

> Airbnb (estimated) expense for a year: ₹12lk ~ $16k

Our stay at Homebase-Alpha has by far been much better than even the nicest airbnbs we have stayed in, India as well as outside. **Considering the cost of remodeling (₹7 lk ~ $9400) the studios and 1 year's rent (₹2.5 lk ~ $3355), our 2020 housing expense was about ₹9.5 lk ~ $13000.**

> Himalayan home Expense for a year: ₹9.5lk ~ $13k

Clearly, building a Himalayan home base was cheaper and more satisfying 😇