# Hendo Homestead

## Timeline Overview

  |     Task                     |  Start     |  Time   |
  | ---------------------------- | ---------- | ------- |
  | Search and Purchase          | Mar 2022   | 1 month |
  | Driveway Permit              | May 2022   | 2 days  |
  | Soil Test Appt & Report      | May 2022   | 3 weeks |
  | Septic Permit                | Jun 2022   | 1 month |
  | Driveway Construction        | Aug 2022   | 1 week  |
  | Certified Site Map           | Aug 2022   | 3 weeks |
  | Building Permit              | Aug 2022   | 1 week  |
  | Electric Pedestal Inspection | Sep 2022   | 1 day   |
  | Electric Road to Pedestal    | Sep 2022   | 2 weeks |
  | Septic Installation          | Oct 2022   | 3 days  |
  | Footer Inspection            | Oct 2022   | 1 day   |
  | Concrete Foundation          | Oct 2022   | 1 week  |
  | Foundation Inspection        | Nov 2022   | 1 day   |
  | Framing                      | Feb 2023   | 4 days  |
  | Mechanical and Plumbing      | Feb 2023   | 1 month |
  | Rough-in Inspection          | Mar 2023   | 1 day   |
  | Insulation and Fire Caulking | Apr 2023   | 2 week  |
  | Fire/Draft Inspection        | Apr 2023   | 1 day   |
  | Interior finish              | Apr 2023   | 2 month |
  | Final Inspection |



## Background

Bo and I returned to USA in 2021 with a plan to settle down close to family and started searching for a country home with 5+ acres. We made an offer on a house in March of 2022 and found out we could not get home loan, despite our amazing credit score and sufficiently high income 🤯 because IRS had not processed our taxes from last 3years. After few back and forth with IRS we had a difficult choice to make -
1. Resume our home search after 3 months and trust the IRS on updated timeline for processing our tax returns.
  OR
2. Use our available savings to buy land and build a smaller home with a plan to extend few years down the line.

We did the obvious - NEVER TRUST GOVERNMENT ON GETTING THINGS DONE ON TIME 😂

## The Search

On March 15, 2022 we decided to take a drive through a potential land option we found on Zillow. The land we originally came to check was good but neither of us felt this is 'it'. There was another lot in the neighbourhood for sale and though it appeared unappealing in the listing, we decided to check it out. We fell in love at the first sight 😍  A beautiful 15 acre lot with gentle slopes, some meadows and some forest cover  - just what we were looking for. We called our Realtor and got things in motion.

![](/ipfs/QmRuTq9YggBC1AezykzwjgsrWtemRXGKvcUy2fmA8XYXDi)
*18 March, 2022*

It took almost a month to get all the paper work - offer letter, escrow setup, title assurance, tax clearance etc. And we were proud owner of this beautiful land.
![](/ipfs/QmPqR6SkscMxT3oeAvYM4966iUxqqux5W2sgxJRSVtx3u1)
*12 April, 2022*

## The Permits

First step while building anything is to find out local building permit requirements. While the process can be daunting for the first time builders, it was not as bad as I expected. We were able to find most of the info on our county's building department website. Anytime we were confused, a quick visit to the building department office provided us with all the answers.

For our project we visited [Monroe County Building Department - Residential Building Permit](https://monroecountyin.viewpointcloud.com/categories/1083/record-types/6442)

Requirements:

- Construction Plans
- Certified Plot Plan
- Site Plan

Since, it was an unimproved land outside the limit of city sewer and we had to approach entrance to our lot from public we also needed:

- Driveway Permit
- Septic Permit

All applications for permit was submit online on [Monroe County, IN website](https://monroecountyin.viewpointcloud.com)

### Driveway Permit

Our first step was to establish access from road to the build site -  *Driveway*. It took us 2 days to complete all requirements and another 2 days to receive the permit after submitting application.

Driveway permit application requirements:
  - Site map of our property (we used the surveyor map we received as part of the land purchase package) showing:
    - Property boundaries
    - Proposed location and width of the driveway (we opted for standard 12' driveway)
    - Distance from the nearest property line to center of driveway
  - Staked out center point of our driveway location (from the road) with vibrant colored flags for site inspection

Even though we received our driveway permit in May, we could not start building it right away because:
  1. Our property was impassable after the April shower. We had 6' tall grass that needed to be bush hogged before we could scout the driveway path and the potential build site.
  2. There were some huge bushes and small trees along driveway path and septic site that needed to be removed before construction
  3. We needed soil to be dry enough before excavators could start digging and this year April showers continued to May and June. So excavators were pretty busy with backlog jobs.

 
### Septic Permit

### Residential Building Permit


## Construction and Land Improvement

### Land Clearing

Our contractor bush hogged area around the driveway path and potential building site and we hired tree experts to take down some of the trees. The entire tree clearing process was completed within day.

![](/ipfs/QmeFmaxA8ug9Go647uih54kQ8quRSJt9qu8GWyTkkegmVH)
*20 July, 2022 Freshly bush hogged/cleared driveway path and build site*


### Driveway Construction

Actual process of driveway construction was 3 full day worth of job, but it took us 1 week to complete because of unexpected rain on the first day just after 40mins of digging. Anyhow, 3 months after receiving the driveway permit we finally had our driveway.

![](/ipfs/QmfZ9DY3N2oZAzygaPduQau2jJefFtYoo1rYidgeVU2iPE)
*Digging dirt and laying gravel*
  

![](/ipfs/QmStcqHRKuxn6wY3RMzG8DDnUqGgsuQHfv68LL1E1rzrWP)
*Final Driveway*

### Electric

The electricity was available on our property at road and to get it to the build site we needed our utility company, REMC, to extend the lines. Since, wait time with utility company were long in our area, we scheduled the appointment with their team month in advance, while we got the electric pedestal hooked up by our electrician and inspected by the Monroe County Building Department.

Cost of extending electric from road to build site was based per foot trench dug and wire laid. Once the team arrived, the whole process was done within a day and costed us $6000 (trench and wire) + $2000 (for transformer and pedestal)
 
![](/ipfs/QmVvZfvfypNEBwaESqQzEYHzjHUUm54eW3dajVX7sBHXxx)
*Electric Pedestal Ready for Inspection*

### Septic Installation
Due to more rain than usual there were a lot of backlog of excavation and septic install jobs. We had to go with a different installer than originally mentioned on our permit application and had to wait 3 months before installer, certified by Monroe County, was available to do the job. Though, once the installer was available it was a matter of just 2 days and our septic field was ready. Our installer left a little marker and told us he would come to install the tank after the heavy machinery work - rest of the excavation and framing - is done.

![](/ipfs/QmZPvMVNDJxrqdeFp4CXNC2undviKiyStchWruCFoaCKgH)
*Freshly installed septic field. The green pipe sticking out is where our septic tank would go later*

### Excavation

![](/ipfs/QmVycZxdoSwN6MybcgN2H1Yt1HTAtzwCBzXFGBesZ6vJdA)

![](/ipfs/QmY7i5dKGWAoFHGWoWohszNWtMQ9t76rnXiekyGa8EaNsP)

![](/ipfs/Qmb1ksdddGbwMa2mnLMu8e3ChzEvi4mxDbzSSxFuNiJ3Ng)
*Bo got to try excavator*

## Foundation

![](/ipfs/QmTPrpaGw87oNwBmEYBr2TowF4PhaNs9knq5B32udJsJzy)
*Bradley's laying down footers*

![](/ipfs/QmTAeqeSo2mjKvQ2PCXCAnEWufL1W6D9bWzujZViNWMDkk)
*Pouring concrete for concrete retaining wall footers*

![](/ipfs/QmT3dCD7f97QoLyyqFNPiwZumy2M5Pz1uzak4KmsxYGYBK)
*Cured concrete with rebar added*

![](/ipfs/QmXJReqxTPZic7wu2fKbrqLnR1PeNMKossujmBvx9xBfVw)
*Poured concrete retaining wall*

![](/ipfs/QmYED3qVM9Uwhcrhu9JkjpKftvqvKQBuLFgcVZg4Pd2aPF)
*Pouring footer for remaining two basement walls*


![](/ipfs/QmcbPL1ChsKfaw4fvRveLCdrAuH2KyLqbVTnirQRJGsWw3)
*Poured concrete foundation*

![](/ipfs/QmVeq1rQ1cgxNkeRxaZFrDKuCxTbVpdYTvhjawv6bVnYVD)
*White pipe sticking out vertically on the corner is connecting to the sewage pipe laid under the foundation. The huge cross mark on the ground is formed by the trenches dug for sewage pipe connecting to septic tank and perimeter drain pipe that would drain rain water away from from the septic tank*

### Framing

### 



