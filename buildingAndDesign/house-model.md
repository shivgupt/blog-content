## Full Model
![](/ipfs/QmUcJ6YrwzkUeDSA2eL3hSQzEFwQUcAwEbReoV86uvNTyT)

## First Floor
![](/ipfs/QmPbrZV88Z9Ah98cVATbuQMuLU3GrufDk8ZDA6PX4CAnSG)


### Kitchen
![](/ipfs/QmRJgjeGztfVpBogATwge1aqFWg5NBzU2iTLH4LXLQCFQX)


## Loft
![](/ipfs/QmWmpUTnQfHZtZMhiatXJtF2fWrm7CBNWbREwecPt8bw8H)

## Basement
![](/ipfs/QmQ4tCynDmpgxrxtLLKZp2GQ7oarBiYR5w28xqyPx5DY3t)
