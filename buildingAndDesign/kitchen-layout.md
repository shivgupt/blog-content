# Kitchen Layout

## Kitchen Cabinet Dimensions
![](/ipfs/QmT6gYzQmmt32VEPYon7wNTxiAXdevjqpfxYkjziyJ5T1T)

## Cabinet Animation

Animation takes 30s to load first time and will appear below 👇🏽

![](/ipfs/QmVak3YFA2qJke6eTJTP7hNGDvbHci1cnpwka3YKKoAaoP)

