Bo and I are excited to welcome our little one this summer. 

Inspired by blogs from few minimalist parents and the book - 'The Montessori Baby', I'm trying to put together things that we would need to be *baby ready*. Hopefully this can be helpful for our friends and family in deciding gifts for the 👶🏻🌧

## Baby Clothes
We would like to dress our baby in natural fabric - cotton, linen, wool.
Also, if you have used baby clothes to gift, we would love it. In India it is a  tradition that a new-born is dressed in used clothes for first few days/weeks of their life. New parents get clothes from friends and family that older kids/grandkids wore.

## Cloth Diapers
We would like pocket and/or fitted diapers with cotton or bamboo insert/inner.

## Beef Tallow Balm
We would like to try using natural products like tallow as our diaper cream as well as massage cream

## Contribution to baby fund
Its hard to know what all we would need in advance, so a gift card or plain old cash contribution would help us navigate as we go

## Contribution to Amazon registry
 
Link 👉🏻 [Amazon baby registry](https://www.amazon.com/baby-reg/shivani-henderson-april-2024-bloomington/LOA433G76CHR?ref_=cm_sw_r_cp_ud_dp_BCE5KEZ2AWW49HAWSJGXl)

## We prefer books over cards
Baby books from Half-Priced Books cost about same as a greeting card and we'd love it if you write a note inside the book. Our baby will get some entertainment from this even before he's able to read, and a book is sturdier so your handwritten message is more likely to last long enough for our son to read it himself.